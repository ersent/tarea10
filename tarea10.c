#include <stdio.h>
int main(int argc, char const *argv[]) {

  int a=0;
  int b=0;
  int c=0;

  printf("\tPrograma que muestra el mayor de tres números\n");
  printf("Escribe el primer número entero: ");
  scanf("%d",&a);
  printf("Escribe el segundo número entero: ");
  scanf("%d",&b);
  printf("Escribe el tercer número entero: ");
  scanf("%d",&c);

  if (a>=b && a>=c) {
    printf("El número mayor es: %d\n", a);
  }
  else{
    if (b>c) {
      printf("El número mayor es: %d\n", b);
    }
    else{
        printf("El número mayor es: %d\n", c);
    }
  }

  return 0;
}
